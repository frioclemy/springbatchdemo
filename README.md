:star2: PROJET SpringBatchDemo  
==============================  
  
Ce projet a pour objectif de découvrir Spring Batch.  
Dans cet exemple nous allons lire un fichier CSV pour écrire son contenu dans une table MySQL.  
  
:pencil2: Schema de fonctionnement de Spring Batch  
--------------------------------------------------  
  
![Image Schema Spring Batch](images/schemaSpringBatch.PNG)  
  
:pencil2: Configuration du projet    
---------------------------------
  
Le projet a été créé depuis l'archetype **Maven quickstart**.  
Un fichier CSV qui contiendra les données que l'on souhaite écrire dans notre base de données MySQL.  
Un fichier de configuration **database.xml** qui va définir un bean "datasource" pour notre base de données.  
  
![Image database.xml](images/databaseXML.PNG)  
Vérifier les informations **url, username et password**.  
  
Le fichier **context.xml** définit `jobRepository` et `jobLauncher`.  
  
Enfin le fichier **job-report.xml** configure le job Spring batch. Il définit un job qui lit le fichier report.csv, le fait correspondre 
à un pojo `Report` et écrit les données dans la base de données MySQL.  

La classe Java **Report** contient le pojo qui permettra de faire la correspondance entre le fichier CSV et la base de données.  
  
Pour l'exécution du job Spring Batch, nous utiliserons la classe **App**.  
  
**Documentation officielle Spring Batch** : [Doc Spring Batch](https://docs.spring.io/spring-batch/trunk/reference/html/index.html)